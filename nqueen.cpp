#include <bits/stdc++.h>

#define N 8

using namespace std;

bool SolveNQ(int** mat, int col, int size) {
    
    // basis rekursif
    if (col >= size) 
        return true;


    // iterasi kolom 'col' pada tiap baris
    for (int i = 0; i < size; i++) {

    }
    

    // tidak ditemukan solusi n queen
    return false;
}

int main() {

    int n;
    cin >> n;

    int** mat;
    memset(mat, 0, sizeof(mat[0][0]) * n * n);

    bool solved;

    solved = SolveNQ(mat, 0, n);

    return 0;
} 