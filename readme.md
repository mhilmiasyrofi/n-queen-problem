# N Queen Problem Solver

Sebuah program c++ sederhana untuk menyelesaikan permasalahan N Queen dengan Backtracking. Program ini dibuat sebagai syarat GDP Labs internship.

## Penggunaan Program

Program akan membaca sebuah bilangan bulat N. Program kemudian mencari solusi N queen problem. Jika ditemukan sebuah solusi, maka solusi ditampilkan pada terminal. Jika solusi tidak ditemukan maka ditampilkan pemberitahuan.